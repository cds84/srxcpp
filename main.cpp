#include <iostream>

#include <stdexcept>
#include <memory>

#include <type_traits>

int main() {return 0;}

/*
class ISubscription {
public:
    virtual ~ISubscription() {}
};

class Disposable : public IDisposable {

    template<typename T>
    class CreateDisposable_ : public IDisposable {
        std::decay_t<T> action;
    public:
        CreateDisposable_(T && action) : action(action) {}
        ~CreateDisposable_() {
            action();
        }
    };

    class EmptyDisposable_ : public IDisposable {
    };

public:

    template<typename T>
    static std::unique_ptr<IDisposable> Create(T && action) {
        return std::make_unique<CreateDisposable_>(action);
    }

    static std::unique_ptr<IDisposable> Empty() {
        return std::make_unique<EmptyDisposable_>();
    }
};

template<typename T>
class IObserver {
public:
    virtual void OnNext(const T &value) = 0;
    virtual void OnError(error::Error &error) = 0;
    virtual void OnCompleted() = 0;
};

template<typename T>
class IObservable{
public:
    virtual IDisposable Subscribe(IObserver<T> observer) = 0;
};
*/
