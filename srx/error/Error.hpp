//
// Created by cds on 28/10/2020.
//

#pragma once

#include <type_traits>
#include <utility>
#include <memory>
#include <optional>

namespace srx::error {

    namespace detail {

        class IError {
        public:
            virtual ~IError() = default;
            virtual void Throw() const = 0;
        };

        template<class T>
        class Error : public IError {
            std::decay_t<T> error;
        public:
            Error(T && error) : error(std::move(error)) {}
            void Throw() const override {
                throw error;
            }
            const auto& Get() {
                return error;
            }
        };
    }

    class Error {

        std::shared_ptr<detail::IError> error;

    public:

        Error(const Error &e) noexcept = default;

        template<class T>
        Error(T && t,
              std::enable_if_t<!std::is_same_v<Error, std::decay_t<T>>, nullptr_t> _ = nullptr)
        {
            error = std::make_shared<detail::Error<std::decay_t<T>>>(std::forward<T>(t));
        }

        Error() {
            error = std::make_shared<detail::Error<std::exception>>(std::exception());
        }

        template <class T>
        bool IsType() const {
            return (bool)std::dynamic_pointer_cast<detail::Error<T>>(error);
        }

        template<class T>
        std::optional<T> Get() const {

            auto p = std::dynamic_pointer_cast<detail::Error<T>>(error);
            if(p) {
                return p->Get();
            }
            return std::nullopt;
        }

        void Throw() {
            error->Throw();
        }
    };
}
