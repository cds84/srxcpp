//
// Created by chris on 24/10/2020.
//

#pragma once

#include <srx/core/Unit.hpp>
#include <srx/core/Observable.hpp>
#include <srx/core/IObserver.hpp>

#include <srx/operators/OperatorBase.hpp>
#include <srx/subject/Subject.hpp>

#include <srx/operators/SubscribeOn.hpp>

//#include <srx/debug/pretty_type_name.hpp>

#include <memory>
#include <tuple>
#include <mutex>

namespace srx::pipe {

    namespace detail {

        template<class OutT, class InT, class... Operators>
        class Pipe : public core::IObservable<OutT> {
            std::shared_ptr<core::IObservable<InT>> source;
            std::tuple<std::decay_t<Operators>...> operators;

            // Own any schedulers that 'Subscribe()' instantiates.
            std::mutex schedulers_mutex;
            std::set<std::shared_ptr<concurrency::ISchedule>> schedulers;

        public:

            Pipe(const std::shared_ptr<core::IObservable<InT>> &source, Operators&&... operators) :
                source(source),
                operators(std::move(operators)...)
            {}

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<OutT>> observer) override {

                if constexpr (0 == sizeof...(Operators))
                    return source->Subscribe(observer);
                else
                    return OnSubscribe<sizeof...(Operators)-1>(observer, {}, observer);
            }

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<OutT>> observer,
                                                 [[maybe_unused]] std::shared_ptr<concurrency::ISchedule> IGNORED) override {
                return Subscribe(observer);
            }

        private:

            /***
             * Get Operator Type from Operator Index.
             */
            template<int Index>
            using OperatorFactoryType = typename std::tuple_element_t<Index, decltype(operators)>;

            /***
             * Get Input type from Operator Index.
             */
            template<int Index>
            using OperatorInputType = typename OperatorFactoryType<Index>::InputType;

            /***
             * Check if Operator Index is a SubScribeOn operator.
             */
            template <int Index>
            static constexpr bool is_subscribe_on_v = std::is_same_v<
                    OperatorFactoryType<Index>,
                    operators::detail::SubscribeOnFactory<OperatorInputType<Index>>>;


            template<int Index, typename OpT>
            subscription::Subscription OnSubscribeParent(
                    std::shared_ptr<core::IObserver<OutT>> observer,
                    std::shared_ptr<concurrency::ISchedule> subscribe_scheduler,
                    const OpT & op) {

                if constexpr (Index > 0)
                    return OnSubscribe<Index - 1>(observer, subscribe_scheduler, op);
                else {
                    return (subscribe_scheduler) ?
                           source->Subscribe(op, subscribe_scheduler) :
                           source->Subscribe(op);
                }
            }

            /***
             * If this stage of the pipe is a special "SubScribeOn" operator,
             * Then extract and forward its scheduler for future use.
             * Don't subscribe to it.
             ***/
            template<int Index, typename ChildT>
            std::enable_if_t<is_subscribe_on_v<Index>, subscription::Subscription>
            OnSubscribe(std::shared_ptr<core::IObserver<OutT>> observer,
                                                   std::shared_ptr<concurrency::ISchedule> subscribe_scheduler,
                                                   const ChildT & child) {

                subscribe_scheduler =
                        std::dynamic_pointer_cast<
                                operators::detail::SubscribeOnOperator>(
                                std::get<Index>(operators)())
                                ->GetScheduler();

                {
                    std::scoped_lock mock(schedulers_mutex);
                    schedulers.insert(subscribe_scheduler);
                }

                return OnSubscribeParent<Index>(observer, subscribe_scheduler, child);
            }

            /***
             * If this stage of the pipe is a regular operator,
             * Then subscribe the child to it, and recurse to next stage.
             ***/
            template<int Index, typename ChildT>
            std::enable_if_t<!is_subscribe_on_v<Index>, subscription::Subscription>
            OnSubscribe(std::shared_ptr<core::IObserver<OutT>> observer,
                        std::shared_ptr<concurrency::ISchedule> subscribe_scheduler,
                        const ChildT & child) {

                auto op = std::get<Index>(operators)();

                if (child) {
                    if (subscribe_scheduler)
                        op->Subscribe(child, subscribe_scheduler);
                    else
                        op->Subscribe(child);
                }
                return OnSubscribeParent<Index>(observer, subscribe_scheduler, op);
            }
        };

        template<typename... Ts>
        struct select_last
        {
            template<typename T>
            struct tag
            {
                using type = T;
            };
            // Use a fold-expression to fold the comma operator over the parameter pack.
            using type = typename decltype((tag<Ts>{}, ...))::type;
        };
    }


    template<
            class InT,
            class... Operators,
            class OutT = typename detail::select_last<Operators...>::type::OutputType>
    core::Observable<OutT>
    Pipe(
            const core::Observable<InT> & source,
            Operators&&... operators) {

        return core::Observable<OutT>::template Wrap<detail::Pipe<OutT, InT, Operators...>>(
                source,
                std::forward<Operators>(operators)...);
    }
}
