//
// Created by cds on 16/11/2020.
//

#pragma once

#include <string>
#include <typeinfo>

#ifdef __GNUG__
#include <cstdlib>
#include <memory>
#include <cxxabi.h>
#endif

namespace srx::debug {

#ifdef __GNUG__
    static std::string demangle(const char *name) {

        int status = 0;

        std::unique_ptr<char, void (*)(void *)> res{
                abi::__cxa_demangle(name, NULL, NULL, &status),
                std::free
        };

        return (status == 0) ? res.get() : name;
    }
#else
    static std::string demangle(const char *name) {
        return name;
    }
#endif

    template<class T>
    std::string pretty_type_name(const T &t) {

        return demangle(typeid(t).name());
    }

    template<class T>
    std::string pretty_type_name() {

        return demangle(typeid(T).name());
    }
}
