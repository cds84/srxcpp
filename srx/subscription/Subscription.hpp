//
// Created by chris on 22/10/2020.
//

#pragma once

#include "ISubscription.hpp"
#include "ActionSubscription.hpp"
#include "EmptySubscription.hpp"

#include <memory>
#include <mutex>

namespace srx::subscription {

    class Subscription {

        std::unique_ptr<ISubscription> subs;

    public:

        Subscription() = default;

        explicit Subscription(std::unique_ptr<ISubscription> && subs) noexcept : subs(std::move(subs)) {}

        Subscription(Subscription && subs) noexcept = default;

        Subscription& operator = (Subscription && subs_) noexcept = default;

        explicit operator bool() const {

            return static_cast<bool>(subs);
        }

        void Unsubscribe() {
            if(subs) {
                subs->Unsubscribe();
                subs.reset();
            }
        }

        void reset() {

            subs.reset();
        }
    };

    template<typename T, typename ...Args>
    inline Subscription Create(Args&&... args) {
        return Subscription(std::make_unique<T>(std::forward<Args>(args)...));
    }

    inline auto Empty() {
        return Create<EmptySubscription>();
    }

    template<typename ActionT>
    inline auto Action(ActionT && action) {
        return Create<ActionSubscription<ActionT>>(std::forward<ActionT>(action));
    }
}