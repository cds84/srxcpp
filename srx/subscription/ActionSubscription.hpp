//
// Created by chris on 22/10/2020.
//

#pragma once

#include "ISubscription.hpp"

#include <type_traits>
#include <utility>

namespace srx::subscription {

    template<typename Action>
    class ActionSubscription : public ISubscription {

        std::decay_t<Action> action;

    public:

        ActionSubscription(Action && action) : action(std::move(action)) {}

        ~ActionSubscription() override {}

        void Unsubscribe() override {
            action();
        }
    };
}
