//
// Created by chris on 24/10/2020.
//

#pragma once

#include <srx/subscription/Subscription.hpp>

namespace srx::subscription {

    class SubscriptionGuard {

        Subscription subs;

    public:

        explicit SubscriptionGuard(Subscription && s) noexcept : subs(std::move(s)) {}

        ~SubscriptionGuard() {

            subs.Unsubscribe();
        }
    };

    typedef std::unique_ptr<SubscriptionGuard> UniqueSubscriptionGuard;

    UniqueSubscriptionGuard MakeSubscriptionGuard(Subscription && s) {

        return std::make_unique<SubscriptionGuard>(std::move(s));
    }
}
