//
// Created by chris on 22/10/2020.
//
#pragma once

namespace srx::subscription {
    struct ISubscription {
        virtual ~ISubscription() {}
        virtual void Unsubscribe() = 0;
    };
}
