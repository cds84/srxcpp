//
// Created by chris on 22/10/2020.
//

#pragma once

#include "ISubscription.hpp"

 namespace srx::subscription {
    struct EmptySubscription : public ISubscription {
        ~EmptySubscription() override = default;
        void Unsubscribe() override {}
    };
}
