//
// Created by cds on 11/11/2020.
//

#pragma once

#include <functional>

namespace srx::concurrency {

    class ISchedule {
    public:
        virtual ~ISchedule() {}
        virtual void Schedule(std::function<void()> f) = 0;
    };
}