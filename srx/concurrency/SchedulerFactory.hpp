//
// Created by cds on 13/11/2020.
//

#pragma once

#include <srx/concurrency/ISchedulerFactory.hpp>

#include <memory>

namespace srx::concurrency {

    class SchedulerFactory {
        std::shared_ptr<detail::ISchedulerFactory> schedulerFactory;
    public:

        // SchedulerFactory can implicitly output Schedulers
        //operator srx::concurrency::Scheduler() const { return this->operator()(); }

        explicit SchedulerFactory(std::shared_ptr<detail::ISchedulerFactory> schedulerFactory) :
            schedulerFactory(std::move(schedulerFactory)) {}

        srx::concurrency::Scheduler operator()() const {

            return schedulerFactory->operator()();
        }
    };
}
