//
// Created by cds on 13/11/2020.
//

#pragma once

#include <srx/concurrency/Scheduler.hpp>

namespace srx::concurrency::detail {

    class ISchedulerFactory {
    public:
        virtual srx::concurrency::Scheduler operator()() const = 0;
    };
}
