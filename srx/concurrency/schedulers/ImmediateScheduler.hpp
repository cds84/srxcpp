//
// Created by cds on 11/11/2020.
//

#pragma once

#include <srx/concurrency/SchedulerFactory.hpp>

#include <memory>

namespace srx::concurrency {

    namespace detail {
        class ImmediateScheduler : public ISchedule {
        public:
            void Schedule(std::function<void()> f) override {
                f();
            }
        };

        class ImmediateSchedulerFactory : public detail::ISchedulerFactory {
        public:
            srx::concurrency::Scheduler operator()() const override {

                #if __cplusplus < 201103L
                    #error manual locking required in compilers older that c++11.
                #else
                    // since c++11 static initialisation is thread safe.
                    static auto is = std::make_shared<detail::ImmediateScheduler>();
                #endif
                return Scheduler(is);
            }
        };
    }

    static SchedulerFactory ImmediateScheduler() {

        #if __cplusplus < 201103L
            #error manual locking required in compilers older that c++11.
        #else
            // since c++11 static initialisation is thread safe.
            static auto isf = std::make_shared<detail::ImmediateSchedulerFactory>();
        #endif
        return SchedulerFactory(isf);
    }
}
