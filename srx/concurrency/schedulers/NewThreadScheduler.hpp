//
// Created by cds on 17/11/2020.
//

#pragma once

#include <srx/concurrency/SchedulerFactory.hpp>

#include <thread>
#include <mutex>
#include <memory>
#include <list>

namespace srx::concurrency {

    namespace detail {

        class NewThreadScheduler : public ISchedule {

            std::mutex mutex;
            std::list<std::thread> list;

        public:

            virtual ~NewThreadScheduler() {

                std::lock_guard guard(mutex);
                for(auto & t : list) {
                    if(t.joinable())
                        t.join();
                }
                list.clear();
            }

            void Schedule(std::function<void()> f) override {

                std::lock_guard guard(mutex);
                list.emplace_back(f);
            }
        };

        class NewThreadSchedulerFactory : public detail::ISchedulerFactory {
        public:
            srx::concurrency::Scheduler operator()() const override {
                return Scheduler(std::make_shared<NewThreadScheduler>());
            }
        };
    }

    static SchedulerFactory NewThreadScheduler() {

        return SchedulerFactory(std::make_shared<detail::NewThreadSchedulerFactory>());
    }
}
