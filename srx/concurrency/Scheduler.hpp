//
// Created by cds on 13/11/2020.
//

#pragma once

#include <srx/concurrency/IScheduler.hpp>

#include <memory>

namespace srx::concurrency {

    class Scheduler {
        std::shared_ptr<ISchedule> schedule;
    public:

        // Schedule can implicitly be converted to std::shared_ptr<ISchedule>
        operator std::shared_ptr<ISchedule>() const { return schedule; }

        explicit Scheduler(std::shared_ptr<ISchedule> schedule) : schedule(std::move(schedule)) {}

        template<typename T>
        void
        Schedule(const T & t) {
            schedule->Schedule(t);
        }
    };
}
