//
// Created by chris on 22/10/2020.
//

#pragma once

#include <srx/core/Observable.hpp>

namespace srx::factory {

    namespace detail {
        template<typename T, typename FuncT>
        class Create : public core::IObservable<T> {
            std::decay_t<FuncT> func;
        public:
            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) override {
                func(observer);
                return subscription::Empty();
            }

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer,
                                                 std::shared_ptr<concurrency::ISchedule> sched) override {

                auto f = func;

                sched->Schedule([f, observer] () {
                    f(observer);
                });

                return subscription::Empty();
            }

            Create(FuncT && f) : func(std::move(f)) {}
        };
    }

    template<typename T, typename FuncT>
    core::Observable<T> Create(FuncT && f) {

        return core::Observable<T>::template Wrap<detail::Create<T, FuncT>>(std::forward<FuncT>(f));

    }
}
