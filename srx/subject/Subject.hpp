//
// Created by chris on 28/10/2020.
//

#pragma once

#include <srx/subject/ISubject.hpp>
#include <srx/subscription/Subscription.hpp>
#include <srx/core/Observable.hpp>

#include <set>

namespace srx::subject {

    namespace detail {

        template<class T>
        class SubjectImpl : public subject::ISubject<T>, public std::enable_shared_from_this<SubjectImpl<T>> {

        protected:

            SubjectImpl() = default;

            std::mutex mutex;
            std::set<std::shared_ptr<core::IObserver<T>>> observers;
            enum class State {
                Active,
                Completed,
                Error,
            } state = State::Active;


            // Static so that it cannot access members.
            //  Parameters must be allocated on the stack.
            //  Stack only - thread-safe.
            static void OnNext_(const decltype(observers) &obs_copy_on_stack, const T &value_on_stack) {

                for (const auto &o : obs_copy_on_stack) {
                    try {
                        o->OnNext(value_on_stack);
                    }
                    catch (...) {}
                }
            }

            static void OnError_(const decltype(observers) &obs_copy_on_stack, const error::Error &err_on_stack) {

                for (const auto &o : obs_copy_on_stack) {
                    try {
                        o->OnError(err_on_stack);
                    }
                    catch (...) {}
                }
            }

            static void OnCompleted_(const decltype(observers) &obs_copy) {

                for (const auto &o : obs_copy) {
                    try {
                        o->OnCompleted();
                    }
                    catch (...) {}
                }
            }

            subscription::Subscription MakeSubscription(std::shared_ptr<core::IObserver<T>> observer) {

                std::weak_ptr<SubjectImpl<T>> weakSubject = this->shared_from_this();
                std::weak_ptr<core::IObserver<T>> weakObserver = observer;
                return subscription::Action([weakSubject, weakObserver]() {
                                                if (auto s = weakSubject.lock(); s)
                                                    if (auto o = weakObserver.lock(); o) {
                                                        std::lock_guard lockGuard(s->mutex);
                                                        s->observers.erase(o);
                                                    }
                                            }
                );
            }

        public:

            static std::shared_ptr<SubjectImpl<T>> New() {

                return std::shared_ptr<SubjectImpl<T>>(new SubjectImpl<T>());
            }

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) override {

                std::lock_guard g(this->mutex);
                this->observers.insert(observer);

                return MakeSubscription(observer);
            }

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer,
                                                 [[maybe_unused]] std::shared_ptr<concurrency::ISchedule> IGNORED) override {
                return Subscribe(observer);
            }

            void OnNext(const T &value) override {

                decltype(observers) obs_copy;
                {
                    std::lock_guard state_guard(this->mutex);
                    if (this->state != State::Active)
                        return;
                    obs_copy = this->observers;
                }
                OnNext_(obs_copy, value);
            }

            void OnError(const error::Error &error) override {

                decltype(observers) obs_copy;
                {
                    std::lock_guard state_guard(this->mutex);
                    if (this->state != State::Active)
                        return;
                    this->state = State::Error;
                    obs_copy = this->observers;
                }
                OnError_(obs_copy, error);
            }

            void OnCompleted() override {

                decltype(observers) obs_copy;
                {
                    std::lock_guard state_guard(this->mutex);
                    if (this->state != State::Active)
                        return;
                    this->state = State::Completed;
                    obs_copy = this->observers;
                }
                OnCompleted_(obs_copy);
            }
        };
    }

    // Subject<T> wraps the implementation of subject to force its instantiation to come via std::shared_ptr
    template<typename T>
    class Subject {
    public:
        // Subject<T> is implicitly convertible to IObserver<T> and IObservable<T>
        operator std::shared_ptr<core::IObservable<T>>() const { return subject; }
        operator std::shared_ptr<core::IObserver<T>>() const { return subject; }

    private:
        std::shared_ptr<detail::SubjectImpl<T>> subject;

    public:

        typedef T type;

        Subject() : subject(detail::SubjectImpl<T>::New()) {}

        Subject(Subject<T> &&) noexcept = default;
        Subject<T>& operator = (Subject<T> && s)  noexcept = default;

        // There is no reason the implementation cant support copied subjects...
        //  But I think an application is better designed if Subjects are unique?
        //  Is it the API's job to FORCE better design?
        //      Something to think about.
        Subject(const Subject<T>&) = delete;
        Subject<T>& operator = (const Subject<T>&) = delete;

        void reset() { subject.reset(); }
        explicit operator bool() const { return (bool)subject; }

        subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) {

            return subject->Subscribe(observer);
        }
        Subject<T>& OnNext(const T &value) {
            subject->OnNext(value);
            return *this;
        }
        Subject<T>& OnError(const error::Error &error) {
            subject->OnError(error);
            return *this;
        }
        Subject<T>& OnCompleted() {
            subject->OnCompleted();
            return *this;
        }

        core::Observable<T> AsObservable() const {

            return core::Observable<T>::Wrap(subject);
        }
    };
}
