//
// Created by chris on 28/10/2020.
//

#pragma once

#include <srx/subject/Subject.hpp>

namespace srx::subject {

    namespace detail {
        template<class T>
        class BehaviourSubjectImpl : public SubjectImpl<T> {

            std::decay_t<T> last_value;
            error::Error last_error;

        private:

            BehaviourSubjectImpl(T &&default_) : last_value(std::move(default_)) {
            }

        public:

            static std::shared_ptr<BehaviourSubjectImpl<T>> New(T &&default_) {

                return std::shared_ptr<BehaviourSubjectImpl<T>>(new BehaviourSubjectImpl<T>(std::forward<T>(default_)));
            }

            subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) override {

                auto state_copy = SubjectImpl<T>::State::Active;
                std::decay_t<T> value_copy;
                {
                    std::lock_guard g(this->mutex);
                    this->observers.insert(observer);
                    state_copy = this->state;
                    value_copy = this->last_value;
                }

                try {
                    observer->OnNext(value_copy);
                }
                catch (...) {}

                if (state_copy == SubjectImpl<T>::State::Error) {
                    try {
                        observer->OnError(last_error);
                    }
                    catch (...) {}
                }

                if (state_copy == SubjectImpl<T>::State::Completed) {
                    try {
                        observer->OnCompleted();
                    }
                    catch (...) {}
                }

                return SubjectImpl<T>::MakeSubscription(observer);
            }

            void OnNext(const T &value) override {

                decltype(SubjectImpl<T>::observers) obs_copy;
                {
                    std::lock_guard state_guard(this->mutex);
                    if (this->state != SubjectImpl<T>::State::Active)
                        return;
                    this->last_value = value;
                    obs_copy = this->observers;
                }
                SubjectImpl<T>::OnNext_(obs_copy, value);
            }

            void OnError(const error::Error &error) override {

                decltype(SubjectImpl<T>::observers) obs_copy;
                {
                    std::lock_guard state_guard(this->mutex);
                    if (this->state != SubjectImpl<T>::State::Active)
                        return;
                    this->state = SubjectImpl<T>::State::Error;
                    this->last_error = error;
                    obs_copy = this->observers;
                }
                SubjectImpl<T>::OnError_(obs_copy, error);
            }
        };
    }

    // BehaviourSubject<T> wraps the implementation of behaviour subject to force its instantiation to come via std::shared_ptr
    template<typename T>
    class BehaviourSubject {
    public:
        // Subject<T> is implicitly convertible to IObserver<T> and IObservable<T>
        operator std::shared_ptr<core::IObservable<T>>() const { return subject; }
        operator std::shared_ptr<core::IObserver<T>>() const { return subject; }

    private:
        std::shared_ptr<detail::BehaviourSubjectImpl<T>> subject;

    public:

        typedef T type;

        BehaviourSubject(const T & t) : subject(detail::BehaviourSubjectImpl<T>::New(t)) {}
        BehaviourSubject(T && t) : subject(detail::BehaviourSubjectImpl<T>::New(std::forward<T>(t))) {}

        BehaviourSubject(BehaviourSubject<T> &&) noexcept = default;
        BehaviourSubject<T>& operator = (BehaviourSubject<T> && s)  noexcept = default;

        // There is no reason the implementation cant support copied subjects...
        //  But I think an application is better designed if Subjects are unique?
        //  Is it the API's job to FORCE better design?
        //      Something to think about.
        BehaviourSubject(const BehaviourSubject<T>&) = delete;
        BehaviourSubject<T>& operator = (const BehaviourSubject<T>&) = delete;

        void reset() { subject.reset(); }
        explicit operator bool() const { return (bool)subject; }

        subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) {

            return subject->Subscribe(observer);
        }
        BehaviourSubject<T>& OnNext(const T &value) {
            subject->OnNext(value);
            return *this;
        }
        BehaviourSubject<T>& OnError(const error::Error &error) {
            subject->OnError(error);
            return *this;
        }
        BehaviourSubject<T>& OnCompleted() {
            subject->OnCompleted();
            return *this;
        }

        core::Observable<T> AsObservable() const {

            return core::Observable<T>::Wrap(subject);
        }
    };
};
