//
// Created by cds on 31/10/2020.
//

#pragma once

#include <srx/core/IObservable.hpp>
#include <srx/core/IObserver.hpp>

namespace srx::subject {

    template<typename T>
    class ISubject : public core::IObservable<T>, public core::IObserver<T> {
    };
}
