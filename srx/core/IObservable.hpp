//
// Created by chris on 22/10/2020.
//

#pragma once

#include <srx/subscription/Subscription.hpp>
#include <srx/core/IObserver.hpp>
#include <srx/concurrency/IScheduler.hpp>

namespace srx::core {

    template<typename T>
    class IObservable{
    public:
        virtual ~IObservable(){}
        virtual subscription::Subscription Subscribe(std::shared_ptr<IObserver<T>> observer) = 0;
        virtual subscription::Subscription Subscribe(std::shared_ptr<IObserver<T>> observer,
                                                     std::shared_ptr<concurrency::ISchedule> subscribe_on) = 0;
    };
}
