//
// Created by cds on 01/11/2020.
//

#pragma once

#include <srx/core/IObservable.hpp>

namespace srx::core {

    namespace detail {
        template<typename T, typename OnNextT, typename OnCompleteT, typename OnErrorT>
        class ActionObserver : public IObserver<T> {
            std::decay_t<OnNextT> n;
            std::decay_t<OnCompleteT> c;
            std::decay_t<OnErrorT> e;

            enum class State {
                Active,
                Completed,
                Error,
            };

            std::mutex mutex;
            State state = State::Active;

        public:
            ActionObserver(OnNextT && n, OnCompleteT && c, OnErrorT && e) : n(std::move(n)), c(std::move(c)), e(std::move(e)) {}

            void OnNext(const T &v) override  {

                {
                    std::lock_guard g(mutex);
                    if(state != State::Active)
                        return;
                }
                n(v);
            }
            void OnError(const error::Error &ex) override {

                {
                    std::lock_guard g(mutex);
                    if(state != State::Active)
                        return;
                    state = State::Error;
                }
                e(ex);
            }
            void OnCompleted() override {
                {
                    std::lock_guard g(mutex);
                    if(state != State::Active)
                        return;
                    state = State::Completed;
                }
                c();
            }
        };
    }

    template<typename T>
    class Observable
    {
    public:

        typedef T type;

        // Observable<T> is implicitly convertible to IObservable<T>
        operator std::shared_ptr<core::IObservable<T>>() const { return observable; }

    private:
        std::shared_ptr<IObservable<T>> observable;

        explicit Observable(std::shared_ptr<IObservable<T>> o) : observable(o) {}

    public:

        template <class Impl, class ...Args>
        static Observable<T> Wrap(Args&&... args) {

            auto impl = std::make_shared<Impl>(std::forward<Args>(args)...);
            return Observable<T>(impl);
        }

        static Observable<T> Wrap(const std::shared_ptr<IObservable<T>> &observable) {

            return Observable<T>(observable);
        }

        Observable(Observable<T> &&) noexcept = default;
        Observable<T>& operator = (Observable<T> && s)  noexcept = default;

        Observable(const Observable<T> &) = delete;
        Observable<T>& operator = (const Observable<T> &) = delete;

        void reset() { observable.reset(); }
        explicit operator bool() const { return (bool)observable; }

        subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<T>> observer) const {

            return observable->Subscribe(observer);
        }

        template<typename OnNextT, typename OnCompleteT, typename OnErrorT>
        std::enable_if_t<
                std::is_invocable_v<OnNextT, T>  &&
                std::is_invocable_v<OnCompleteT> &&
                std::is_invocable_v<OnErrorT, error::Error>
                    ,subscription::Subscription>
        Subscribe(OnNextT && next, OnCompleteT && complete, OnErrorT && err) const {

            return Subscribe(std::make_shared<detail::ActionObserver<T,OnNextT,OnCompleteT,OnErrorT>>(
                    std::forward<OnNextT>(next),
                    std::forward<OnCompleteT>(complete),
                    std::forward<OnErrorT>(err)));
        }

        template<typename OnNextT, typename OnCompleteT>
        std::enable_if_t<
                std::is_invocable_v<OnNextT, T>  &&
                std::is_invocable_v<OnCompleteT>
                    ,subscription::Subscription>
        Subscribe(OnNextT && next, OnCompleteT && complete) const {

            return Subscribe(std::forward<OnNextT>(next),
                             std::forward<OnCompleteT>(complete),
                             [](const error::Error &){});
        }

        template<typename OnNextT>
        std::enable_if_t<
                std::is_invocable_v<OnNextT, T>
                        ,subscription::Subscription>
         Subscribe(OnNextT && next) const {

            return Subscribe(std::forward<OnNextT>(next),
                             [](){},
                             [](const error::Error &){});
        }
    };
}