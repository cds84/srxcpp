//
// Created by chris on 22/10/2020.
//

#pragma once

#include <srx/error/Error.hpp>

namespace srx::core {

    template<typename T>
    class IObserver {
    public:
        virtual void OnNext(const T &value) = 0;
        virtual void OnError(const error::Error &error) = 0;
        virtual void OnCompleted() = 0;
    };
}
