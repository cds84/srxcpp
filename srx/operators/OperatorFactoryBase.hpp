//
// Created by chris on 28/10/2020.
//

#pragma once

#include "OperatorBase.hpp"
#include <memory>

namespace srx::operators::detail {

    template<class OutputTypeT, class InputTypeT>
    class OperatorFactoryBase {
    public:
        typedef InputTypeT InputType;
        typedef OutputTypeT OutputType;

        virtual std::shared_ptr<OperatorBase<OutputType,InputType>> operator()() const = 0;
    };
}
