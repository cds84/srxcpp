//
// Created by chris on 23/10/2020.
//

#pragma once

#include <srx/core/IObservable.hpp>
#include <srx/core/IObserver.hpp>

#include <mutex>
#include <cassert>

namespace srx::operators::detail {

    template<class OutT, class InT>
    class OperatorBase :
              public core::IObserver<InT>
            , public core::IObservable<OutT>
            , public std::enable_shared_from_this<OperatorBase<OutT, InT>>
    {
        std::shared_ptr<core::IObserver<OutT>> observer;

    protected:

        std::mutex state_mutex;
        enum class State {
            Active,
            Completed,
            Error,
        } state = State::Active;

        void SendToObserver(const OutT & value) {

            {
                std::lock_guard state_guard(state_mutex);
                if (state != State::Active)
                    return;
            }

            if(auto o = observer; o)
                o->OnNext(value);
        }

    public:

        subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<OutT>> observer_) override {

            std::lock_guard<std::mutex> guard(state_mutex);
            assert(!observer);
            this->observer = observer_;
            return subscription::Action([](){
                // I think this can be subscription::Empty().
                // I don't think we ever need to unsubscribe from an operator.
                abort();
            });
        }

        subscription::Subscription Subscribe(std::shared_ptr<core::IObserver<OutT>> observer,
                                             [[maybe_unused]] std::shared_ptr<concurrency::ISchedule> IGNORED) override {
            return Subscribe(observer);
        }

        void OnError(const error::Error &error) override {

            {
                std::lock_guard state_guard(state_mutex);
                if (state != State::Active)
                    return;
                state = State::Error;
            }

            if(auto o = observer; o)
                o->OnError(error);
        }
        void OnCompleted() override {

            {
                std::lock_guard state_guard(state_mutex);
                if (state != State::Active)
                    return;
                state = State::Completed;
            }

            if(auto o = observer; o)
                o->OnCompleted();
        }
    };
}
