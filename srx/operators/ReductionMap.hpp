//
// Created by cds on 24/06/2021.
//

#pragma once

#include <srx/operators/OperatorFactoryBase.hpp>

namespace srx::operators {

    namespace detail {

        template<typename OutT, typename InT, typename ReduceFuncT>
        class ReductionMap : public OperatorBase<OutT, InT> {

            ReduceFuncT reduce_func;

            template<int INDEX>
            OutT reduce_(const InT &inputs) const {

                if constexpr((INDEX+1) < std::tuple_size_v<InT>) {

                    return reduce_func(
                            std::get<INDEX>(inputs),
                            reduce_<INDEX + 1>(inputs));
                }
                else {
                    return std::get<INDEX>(inputs);
                }
            }

            OutT reduce(const InT &values) const {

                return reduce_<0>(values);
            }

        public:

            ReductionMap(ReduceFuncT reduce_func_) : reduce_func(reduce_func_) {

            }

            void OnNext(const InT &values) override {

                const OutT value = reduce(values);

                OperatorBase<OutT,InT>::SendToObserver(value);
            }
        };

        template<class OutT, class InT, class ReduceFuncT>
        class ReductionMapFactory : public OperatorFactoryBase<OutT, InT> {

            ReduceFuncT reduce_func;

        public:

            ReductionMapFactory(ReduceFuncT reduce_func_) : reduce_func(reduce_func_) {
            }

            std::shared_ptr<OperatorBase<OutT, InT>> operator()() const override {
                return std::make_shared<ReductionMap<OutT, InT, ReduceFuncT>>(reduce_func);
            }
        };
    }

    template<
            class InT,
            class ReduceFuncT,
            class ElementT = typename std::tuple_element<0, InT>::type>
    detail::ReductionMapFactory<ElementT, InT, ReduceFuncT>
    ReductionMap(ReduceFuncT reduce_func) {
        static_assert(std::tuple_size_v<InT> > 1);
        return detail::ReductionMapFactory<ElementT, InT, ReduceFuncT>(reduce_func);
    }

    template<
            class InT,
            class ElementT = typename std::tuple_element<0, InT>::type,
            class ReduceFuncT = std::function<ElementT(ElementT,ElementT)>>
    detail::ReductionMapFactory<ElementT, InT, ReduceFuncT>
    AndReductionMap() {

        ReduceFuncT reduce_func = [](ElementT a, ElementT b) { return a && b; };
        return ReductionMap<InT>(reduce_func);
    }

    template<
            class InT,
            class ElementT = typename std::tuple_element<0, InT>::type,
            class ReduceFuncT = std::function<ElementT(ElementT,ElementT)>>
    detail::ReductionMapFactory<ElementT, InT, ReduceFuncT>
    OrReductionMap() {

        ReduceFuncT reduce_func = [](ElementT a, ElementT b) { return a || b; };
        return ReductionMap<InT>(reduce_func);
    }
}
