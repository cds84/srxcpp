//
// Created by cds on 22/11/2020.
//

#pragma once

#include <srx/operators/OperatorFactoryBase.hpp>
#include <srx/concurrency/SchedulerFactory.hpp>

namespace srx::operators {

    namespace detail {

        template<class T>
        class ObserveOnOperator : public OperatorBase<T,T> {
            concurrency::Scheduler sched;
        public:

            explicit ObserveOnOperator(concurrency::Scheduler && s) :
                    sched(std::move(s)) {}

            void OnNext(const T &value) override {

                auto v = value;

                sched.Schedule([v, this](){
                    OperatorBase<T,T>::SendToObserver(v);
                });
            }

            void OnError(const error::Error &error) override {

                auto e = error;

                sched.Schedule([this, e](){
                    this->OperatorBase<T,T>::OnError(e);
                });
            }

            void OnCompleted() override {

                sched.Schedule([this](){
                    this->OperatorBase<T,T>::OnCompleted();
                });
            }
        };

        template<class T>
        class ObserveOnFactory {
            concurrency::SchedulerFactory sf;
        public:

            typedef T InputType;
            typedef T OutputType;

            std::shared_ptr<ObserveOnOperator<T>> operator()() const {
                return std::make_shared<ObserveOnOperator<T>>(sf());
            }

            explicit ObserveOnFactory(concurrency::SchedulerFactory && sf) : sf(std::move(sf)) {}
        };
    }

    template<class T>
    detail::ObserveOnFactory<T> ObserveOn(concurrency::SchedulerFactory && schedFactory) {

        return detail::ObserveOnFactory<T>(std::forward<concurrency::SchedulerFactory>(schedFactory));
    }
}