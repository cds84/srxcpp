//
// Created by cds on 04/11/2020.
//

#pragma once

#include <srx/core/Observable.hpp>
#include <srx/operators/OperatorFactoryBase.hpp>
#include <srx/error/Error.hpp>

namespace srx::operators {

    namespace detail {

        template<class OutT, class InT, class ...AuxInT>
        class CombineLatestOperator : public OperatorBase<OutT, InT> {

            typedef std::tuple<std::decay_t<InT>, std::decay_t<AuxInT>...> LatestTuple;

            std::mutex mutex;
            size_t latest_bitmask = (1UL << (sizeof...(AuxInT) + 1UL))-1UL; // bit-mask of observables yet to call OnNext()
            LatestTuple latest;

            template<size_t INDEX, class T>
            void OnNext_(const T & t) {

                decltype(latest) copy;
                {
                    std::lock_guard g(mutex);
                    std::get<INDEX>(latest) = t;
                    latest_bitmask &= ~(1UL << INDEX);

                    if(latest_bitmask != 0)
                        return; // Not all observables have called this OnNext(), Don't call observer's OnNext.

                    copy = latest;
                }
                OperatorBase<OutT,InT>::SendToObserver(copy);
            }

            template<int AUX_INDEX, int INDEX=AUX_INDEX+1>
            void SubscribeAux(const std::tuple<std::shared_ptr<core::IObservable<AuxInT>>...> & auxInputs) {

                if constexpr (INDEX < std::tuple_size_v<LatestTuple>) {

                    auto p = std::dynamic_pointer_cast<CombineLatestOperator<OutT, InT, AuxInT...>>(
                            this->shared_from_this());

                    core::Observable<std::tuple_element_t<INDEX, LatestTuple>>::Wrap(
                            std::get<AUX_INDEX>(auxInputs)).Subscribe(

                            [p](const std::tuple_element_t<INDEX, LatestTuple> &i) {
                                p->template OnNext_<INDEX>(i);
                            },
                            [p]() {
                                p->OnCompleted();
                            },
                            [p](const error::Error &e) {
                                p->OnError(e);
                            }
                    );
                    SubscribeAux<AUX_INDEX+1>(auxInputs);
                }
            }

            CombineLatestOperator() = default;

        public:

            static std::shared_ptr<CombineLatestOperator<OutT, InT, AuxInT...>>
            New(const std::tuple<std::shared_ptr<core::IObservable<AuxInT>>...> & auxInputs) {

                auto p =  std::shared_ptr<CombineLatestOperator<OutT, InT, AuxInT...>>(
                        new CombineLatestOperator<OutT, InT, AuxInT...>());

                p->template SubscribeAux<0>(auxInputs);

                return p;
            }

            void OnNext(const InT &value) override {

                OnNext_<0>(value);
            }
        };


        template<class OutT, class InT, class ...AuxInT>
        class CombineLatestFactory : public OperatorFactoryBase<OutT, InT> {

            std::tuple<std::shared_ptr<core::IObservable<AuxInT>>...> auxInputs;

            template<size_t INDEX, class ObservableT, class ...TheRestT>
            void Set( const ObservableT& observable, const TheRestT&... theRest) {

                std::get<INDEX>(auxInputs) = observable;
                if constexpr (sizeof...(TheRestT)) {
                    Set<INDEX+1>(theRest...);
                }
            }

        public:

            CombineLatestFactory(const std::shared_ptr<core::IObservable<AuxInT>>&... auxInputs) {

                Set<0>(auxInputs...);
            }

            std::shared_ptr<OperatorBase<OutT,InT>> operator()() const override {

                return CombineLatestOperator<OutT, InT, AuxInT...>::New(auxInputs);
            }
        };
    }

    template<class InT,
            class ...AuxInT,
            class OutT = std::tuple<InT, typename std::decay_t<AuxInT>::type...>>
    detail::CombineLatestFactory<OutT, InT, typename std::decay_t<AuxInT>::type...>
    CombineLatest(const AuxInT&... auxInputs) {

        return detail::CombineLatestFactory<
                OutT,
                InT,
                typename std::decay_t<AuxInT>::type...>(auxInputs...);
    }
}