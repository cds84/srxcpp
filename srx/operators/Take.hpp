//
// Created by chris on 23/10/2020.
//

#pragma once

#include <srx/operators/OperatorFactoryBase.hpp>

#include <cstddef>
#include <cassert>

namespace srx::operators {

    namespace detail {

        template<class T>
        class TakeOperator : public OperatorBase<T,T> {

            std::mutex count_mutex;
            size_t count_to_queue;
            size_t count_to_complete;

        public:

            explicit TakeOperator(size_t count) : count_to_queue(count), count_to_complete(count) {
                assert(count > 0);
            }

            void OnNext(const T &value) override {

                count_mutex.lock();
                const bool valid = count_to_queue > 0;
                if(valid)
                    --count_to_queue;
                count_mutex.unlock();

                if(valid) {

                    OperatorBase<T, T>::SendToObserver(value);

                    count_mutex.lock();
                    count_to_complete--;
                    const bool last = count_to_complete == 0;
                    count_mutex.unlock();

                    if (last)
                        OperatorBase<T, T>::OnCompleted();
                }
            }
        };

        template<class T>
        class TakeFactory : public OperatorFactoryBase<T,T> {
            size_t n;
        public:

            TakeFactory(size_t n) : n(n) {}

            std::shared_ptr<OperatorBase<T,T>> operator()() const override {
                return std::make_shared<TakeOperator<T>>(n);
            }
        };
    }

    template<class T>
    detail::TakeFactory<T> Take(size_t s) {
        return detail::TakeFactory<T>(s);
    }
}
