//
// Created by cds on 13/11/2020.
//

#pragma once

#include <srx/operators/OperatorFactoryBase.hpp>
#include <srx/concurrency/SchedulerFactory.hpp>
#include <utility>

namespace srx::operators {

    namespace detail {

        class SubscribeOnOperator {
            concurrency::Scheduler sched;
        public:

            explicit SubscribeOnOperator(concurrency::Scheduler && s) :
                    sched(std::move(s)) {}

            [[nodiscard]] const concurrency::Scheduler &GetScheduler() const {

                return sched;
            }
        };

        template<class T>
        class SubscribeOnFactory {
            concurrency::SchedulerFactory sf;
        public:

            typedef T InputType;
            typedef T OutputType;

            std::shared_ptr<SubscribeOnOperator> operator()() const {
                return std::make_shared<SubscribeOnOperator>(sf());
            }

            explicit SubscribeOnFactory(concurrency::SchedulerFactory && sf) : sf(std::move(sf)) {}
        };
    }

    template<class T>
    detail::SubscribeOnFactory<T> SubscribeOn(concurrency::SchedulerFactory && schedFactory) {

        return detail::SubscribeOnFactory<T>(std::forward<concurrency::SchedulerFactory>(schedFactory));
    }
}
