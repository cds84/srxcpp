//
// Created by chris on 22/10/2020.
//

#include <srx/factory/Create.hpp>
#include <srx/subject/Subject.hpp>
#include <srx/pipe/Pipe.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::subject;
using namespace srx::factory;
using namespace srx::core;
using namespace srx::pipe;
using namespace srx;

BOOST_AUTO_TEST_SUITE( FactoryCreateSuite )

    BOOST_AUTO_TEST_CASE(resubscribe_completed_observer)
    {
        int nexts{};
        int sum{};
        int errors{};
        int completes{};

        auto c = Create<int>([](auto o){
            o->OnNext(1);
            o->OnNext(2);
            o->OnNext(3);
            o->OnCompleted();
        });

        Subject<int> s;

        s.AsObservable()
            .Subscribe(
                    [&](int i){ sum += i; ++nexts; },
                    [&](){++completes;},
                    [&](const error::Error &) {++errors;});

        c.Subscribe(s);
        BOOST_CHECK(nexts==3);
        BOOST_CHECK(sum==6);
        BOOST_CHECK(completes==1);

        //  This observer is in a completed state!
        c.Subscribe(s);
        BOOST_CHECK(nexts==3);
        BOOST_CHECK(sum==6);
        BOOST_CHECK(completes==1);
    }

    BOOST_AUTO_TEST_CASE(resubscribe_active_observer)
    {
        int nexts{};
        int sum{};
        int errors{};
        int completes{};

        auto c = Create<int>([](auto o){
            o->OnNext(1);
            o->OnNext(2);
            o->OnNext(3);
        });

        Subject<int> s;

        s.AsObservable()
                .Subscribe(
                        [&](auto i){ sum += i; ++nexts; },
                        [&](){++completes;},
                        [&](const error::Error &) {++errors;});

        c.Subscribe(s);
        BOOST_CHECK(nexts==3);
        BOOST_CHECK(sum==6);
        BOOST_CHECK(completes==0);

        c.Subscribe(s);
        BOOST_CHECK(nexts==6);
        BOOST_CHECK(sum==12);
        BOOST_CHECK(completes==0);
    }

    BOOST_AUTO_TEST_CASE(multiple_subscriptions)
    {
        int nexts{};
        int sum{};
        int errors{};
        int completes{};

        auto c = Create<int>([](auto o){
            o->OnNext(1);
            o->OnNext(2);
            o->OnNext(3);
            o->OnCompleted();
        });

        Subject<int> s0, s1;

        s0.AsObservable()
                .Subscribe(
                        [&](auto i){ sum += i; ++nexts; },
                        [&](){++completes;},
                        [&](const error::Error &) {++errors;});

        s1.AsObservable()
                .Subscribe(
                        [&](auto i){ sum += i; ++nexts; },
                        [&](){++completes;},
                        [&](const error::Error &) {++errors;});

        c.Subscribe(s0);
        BOOST_CHECK(nexts==3);
        BOOST_CHECK(sum==6);
        BOOST_CHECK(completes==1);

        //  This observer is in a completed state!
        c.Subscribe(s1);
        BOOST_CHECK(nexts==6);
        BOOST_CHECK(sum==12);
        BOOST_CHECK(completes==2);
    }

BOOST_AUTO_TEST_SUITE_END()
