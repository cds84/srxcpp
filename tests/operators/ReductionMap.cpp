//
// Created by cds on 24/06/2021.
//

#include <srx/operators/ReductionMap.hpp>
#include <srx/operators/CombineLatest.hpp>
#include <srx/pipe/Pipe.hpp>
#include <srx/subject/BehaviourSubject.hpp>
#include <srx/error/Error.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::subject;
using namespace srx::pipe;
using namespace srx::operators;

BOOST_AUTO_TEST_SUITE( ReductionMapSuite )

    BOOST_AUTO_TEST_CASE(and_reduction)
    {
        int val_count{};
        bool val = false;
        int completed{};
        int err{};

        BehaviourSubject<bool> input0(true);
        BehaviourSubject<bool> input1(true);
        BehaviourSubject<bool> input2(true);

        Pipe(input0.AsObservable(),
             CombineLatest<bool>(input1, input2),
             AndReductionMap<std::tuple<bool, bool, bool>>())
                .Subscribe(
                        [&](const auto & i){ val = i; ++val_count;},
                        [&](){++completed;},
                        [&](const auto &e){++err;});

        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,1);

        input1.OnNext(false);
        BOOST_CHECK_EQUAL(val, false);
        BOOST_CHECK_EQUAL(val_count,2);

        input0.OnNext(false);
        BOOST_CHECK_EQUAL(val, false);
        BOOST_CHECK_EQUAL(val_count,3);

        input1.OnNext(true);
        BOOST_CHECK_EQUAL(val, false);
        BOOST_CHECK_EQUAL(val_count,4);

        input0.OnNext(true);
        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,5);
    }

    BOOST_AUTO_TEST_CASE(or_reduction)
    {
        int val_count{};
        bool val = false;
        int completed{};
        int err{};

        BehaviourSubject<bool> input0(true);
        BehaviourSubject<bool> input1(true);
        BehaviourSubject<bool> input2(true);

        Pipe(input0.AsObservable(),
             CombineLatest<bool>(input1, input2),
             OrReductionMap<std::tuple<bool, bool, bool>>())
                .Subscribe(
                        [&](const auto & i){ val = i; ++val_count;},
                        [&](){++completed;},
                        [&](const auto &e){++err;});

        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,1);

        input1.OnNext(false);
        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,2);

        input0.OnNext(false);
        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,3);

        input2.OnNext(false);
        BOOST_CHECK_EQUAL(val, false);
        BOOST_CHECK_EQUAL(val_count,4);

        input1.OnNext(true);
        BOOST_CHECK_EQUAL(val, true);
        BOOST_CHECK_EQUAL(val_count,5);
    }

BOOST_AUTO_TEST_SUITE_END()
