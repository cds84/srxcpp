//
// Created by cds on 23/11/2020.
//

#include <boost/test/unit_test.hpp>

#include <srx/error/Error.hpp>
#include <srx/pipe/Pipe.hpp>
#include <srx/factory/Create.hpp>
#include <srx/operators/ObserveOn.hpp>
#include <srx/concurrency/schedulers/NewThreadScheduler.hpp>

#include <thread>
#include <map>

using namespace srx::pipe;
using namespace srx::factory;
using namespace srx::operators;
using namespace srx::concurrency;
using namespace srx::error;

BOOST_AUTO_TEST_SUITE(ObserveOnSuite)

BOOST_AUTO_TEST_CASE(observe_on) {

        std::map<int,std::thread::id> onNext;
        std::thread::id onComplete;
        std::thread::id onError;

        {
            auto p = Pipe(Create<int>([&](auto o) {
                              o->OnNext(1);
                              o->OnNext(2);
                              o->OnNext(3);
                              o->OnError({});
                              o->OnCompleted();
                          }),
                          ObserveOn<int>(NewThreadScheduler()))
                    .Subscribe([&](const auto &i) { onNext[i] = std::this_thread::get_id(); },
                               [&]() {onComplete = std::this_thread::get_id(); },
                               [&](const Error &e) {onError = std::this_thread::get_id(); });
        }

        for(const auto &[_,t] : onNext)
            BOOST_CHECK(t != std::this_thread::get_id());
        BOOST_CHECK(onComplete != std::this_thread::get_id());
        BOOST_CHECK(onError != std::this_thread::get_id());
    }

BOOST_AUTO_TEST_SUITE_END()
