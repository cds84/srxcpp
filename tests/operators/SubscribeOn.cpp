//
// Created by cds on 13/11/2020.
//

#include <srx/pipe/Pipe.hpp>
#include <srx/operators/SubscribeOn.hpp>
#include <srx/operators/Take.hpp>
#include <srx/factory/Create.hpp>
#include <srx/concurrency/schedulers/ImmediateScheduler.hpp>
#include <srx/concurrency/schedulers/NewThreadScheduler.hpp>

#include <future>
#include <thread>
#include <mutex>
#include <set>

#include <boost/test/unit_test.hpp>

using namespace std::chrono_literals;

using namespace srx::factory;
using namespace srx::operators;
using namespace srx::concurrency;
using namespace srx::pipe;

BOOST_AUTO_TEST_SUITE(SubscribeOnSuite)

    BOOST_AUTO_TEST_CASE(test_new_thread_sched) {

        std::mutex mutex;
        std::set<std::thread::id> threads;

        std::promise<void> promise;
        std::future<void> future = promise.get_future();

        const int thread_count=1000;

        {
            auto p = Pipe(Create<std::thread::id>([&](auto o) {
                              o->OnNext(std::this_thread::get_id());
                          }),
                          SubscribeOn<std::thread::id>(NewThreadScheduler()));

            threads.insert(std::this_thread::get_id());

            for (int i = 0; i < thread_count - 1; ++i) {
                p.Subscribe([&](const std::thread::id &id) {
                    {
                        std::scoped_lock lock(mutex);
                        threads.insert(id);
                        if (threads.size() == thread_count)
                            promise.set_value();
                    }
                    future.wait(); // keep all threads alive to prevent re-use of a thread id.
                });
            }
        }

        {
            std::scoped_lock lock(mutex);
            BOOST_CHECK(threads.size() == thread_count);
        }
    }

BOOST_AUTO_TEST_SUITE_END()
