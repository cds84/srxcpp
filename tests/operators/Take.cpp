//
// Created by chris on 24/10/2020.
//

#include <srx/operators/Take.hpp>
#include <srx/core/Observable.hpp>
#include <srx/pipe/Pipe.hpp>
#include <srx/subject/Subject.hpp>
#include <srx/error/Error.hpp>

#include <boost/test/unit_test.hpp>
#include <atomic>
#include <thread>
#include <future>

//using namespace srx::factory;
using namespace srx::subject;
using namespace srx::core;
using namespace srx::pipe;
using namespace srx::operators;
using namespace srx::error;

BOOST_AUTO_TEST_SUITE( FactoryTakeSuite )

BOOST_AUTO_TEST_CASE(basic)
{
    int total{};
    int completed{};
    int err{};

    Subject<int> input;

    Pipe(input.AsObservable(),
         Take<int>(5))
         .Subscribe(
                 [&total](int i){ total += i;},
                 [&completed](){++completed;},
                 [&err](const Error &e){++err;});

    input.OnNext(1); // 1
    input.OnNext(2); // 3
    input.OnNext(3); // 6
    input.OnNext(4); // 10
    input.OnNext(5); // 15
    input.OnNext(6); // 21
    input.OnNext(7); // 28

    BOOST_TEST(completed == 1);
    BOOST_TEST(err == 0);
    BOOST_TEST(total == 15);
}

BOOST_AUTO_TEST_CASE(threaded)
{
    std::atomic<int> total{};
    std::atomic<int> completed{};
    std::atomic<int> err{};

    Subject<int> input;

    const int thread_count = 50;
    const int take = 50000;

    Pipe(input.AsObservable(),
         Take<int>(take))
            .Subscribe(
                    [&total](int i){ total += i;},
                    [&completed](){++completed;},
                    [&err](const Error &e){++err;});

    std::promise<void> promise;
    std::future<void> future = promise.get_future();

    auto func = [&]() {
        future.wait();
        for(int i=0; i<take; i++)
            input.OnNext(1);
    };

    std::vector<std::thread> threads;
    for(int i=0; i<thread_count; i++)
        threads.emplace_back(func);

    promise.set_value();

    for(auto & t : threads)
        t.join();

    BOOST_TEST(completed == 1);
    BOOST_TEST(err == 0);
    BOOST_TEST(total == take);
}

BOOST_AUTO_TEST_SUITE_END()
