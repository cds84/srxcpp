//
// Created by cds on 04/11/2020.
//

#include <srx/operators/CombineLatest.hpp>
#include <srx/core/Observable.hpp>
#include <srx/pipe/Pipe.hpp>
#include <srx/factory/Create.hpp>
#include <srx/subject/Subject.hpp>
#include <srx/subject/BehaviourSubject.hpp>
#include <srx/error/Error.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::factory;
using namespace srx::subject;
using namespace srx::core;
using namespace srx::pipe;
using namespace srx::operators;
using namespace srx::error;

BOOST_AUTO_TEST_SUITE( CombineLatestSuite )

    BOOST_AUTO_TEST_CASE(test_hot)
    {
        int val_count{};
        std::tuple<int,int,int> val{};
        int completed{};
        int err{};

        Subject<int> input0;
        Subject<int> input1;
        Subject<int> input2;

        auto i1 = input1.AsObservable();

        Pipe(input0.AsObservable(),
             CombineLatest<int>(input1, input2))
                .Subscribe(
                        [&](const auto & i){ val = i; ++val_count;},
                        [&](){++completed;},
                        [&](const auto &e){++err;});

        input0.OnNext(100);
        input1.OnNext(200);
        BOOST_CHECK(val != std::make_tuple(100,200,300));
        BOOST_CHECK(val_count==0);
        input2.OnNext(300);
        BOOST_CHECK(val_count==1);
        BOOST_CHECK(val == std::make_tuple(100,200,300));

        input0.OnNext(101);
        BOOST_CHECK(val_count==2);
        BOOST_CHECK(val == std::make_tuple(101,200,300));
        input1.OnNext(201);
        BOOST_CHECK(val_count==3);
        BOOST_CHECK(val == std::make_tuple(101,201,300));
        input2.OnNext(301);
        BOOST_CHECK(val_count==4);
        BOOST_CHECK(val == std::make_tuple(101,201,301));

        input1.OnError(std::runtime_error("RT Err 1"));

        BOOST_CHECK(err == 1);

        input0.OnNext(999);
        input1.OnNext(999);
        input2.OnNext(999);
        input0.OnError(std::runtime_error("RT Err 1"));
        input1.OnError(std::runtime_error("RT Err 2"));
        input2.OnError(std::runtime_error("RT Err 3"));
        input0.OnCompleted();
        input1.OnCompleted();
        input2.OnCompleted();

        BOOST_CHECK(val_count==4);
        BOOST_CHECK(val == std::make_tuple(101,201,301));
        BOOST_CHECK(err == 1);
        BOOST_CHECK(completed == 0);
    }

    BOOST_AUTO_TEST_CASE(test_cold)
    {
        int val_count{};
        std::tuple<int,int,int> val{};
        int completed{};
        int err{};

        auto c0 = [](auto s){
            s->OnNext(100);
            s->OnNext(101);
            s->OnNext(102);
        };
        auto c1 = [](auto s){
            s->OnNext(200);
            s->OnNext(201);
            s->OnNext(202);
        };
        auto c2 = [](auto s){
            s->OnNext(300);
            s->OnNext(301);
            s->OnNext(302);
        };

        Pipe(Create<int>(c0),
             CombineLatest<int>(Create<int>(c1), Create<int>(c2)))
                .Subscribe(
                        [&](const auto & i){ val = i; ++val_count;},
                        [&](){++completed;},
                        [&](const auto &e){++err;});

        BOOST_CHECK(val_count==3);
        BOOST_CHECK(val == std::make_tuple(102,202,302));
        BOOST_CHECK(err == 0);
        BOOST_CHECK(completed == 0);
    }

    BOOST_AUTO_TEST_CASE(behaviour_subject)
    {
        int val_count{};
        std::tuple<int,int,int> val{};
        int completed{};
        int err{};



        Pipe(BehaviourSubject(1).AsObservable(),
             CombineLatest<int>(BehaviourSubject(2).AsObservable(), BehaviourSubject(3).AsObservable()))
                .Subscribe(
                        [&](const auto & i){ val = i; ++val_count;},
                        [&](){++completed;},
                        [&](const auto &e){++err;});

        BOOST_CHECK(val_count==1);
        BOOST_CHECK(val == std::make_tuple(1,2,3));
        BOOST_CHECK(err == 0);
        BOOST_CHECK(completed == 0);
    }

BOOST_AUTO_TEST_SUITE_END()
