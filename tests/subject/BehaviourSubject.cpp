//
// Created by cds on 29/10/2020.
//

#include <srx/subject/BehaviourSubject.hpp>

#include <memory>

#include <boost/test/unit_test.hpp>

using namespace srx::core;
using namespace srx::subject;
using namespace srx;

BOOST_AUTO_TEST_SUITE( BehaviourSubjectSuite )

    BOOST_AUTO_TEST_CASE(unsubscribe)
    {
        BehaviourSubject<int> subject(100);

        int nexts{};
        int sum{};
        int errors{};
        int completes{};
        error::Error error;

        auto subscription = subject.AsObservable().Subscribe(
                [&](auto i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const error::Error &e) { ++errors; error = e; });

        subject.OnNext(1)
            .OnNext(2)
            .OnNext(3);
        subscription.Unsubscribe();
        subject.OnNext(4);
        subject.OnCompleted();

        BOOST_CHECK(sum==106);
        BOOST_CHECK(completes==0);
        BOOST_CHECK(errors==0);
    }

    BOOST_AUTO_TEST_CASE(complete)
    {
        BehaviourSubject<int> subject(100);

        int nexts{};
        int sum{};
        int errors{};
        int completes{};
        error::Error error;

        auto subscription = subject.AsObservable().Subscribe(
                [&](auto i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const error::Error &e) { ++errors; error = e; });

        subject.OnNext(1);
        subject.OnNext(2);
        subject.OnNext(3);
        subject.OnCompleted();
        subject.OnNext(4);
        subject.OnError({});

        BOOST_CHECK(sum==106);
        BOOST_CHECK(completes==1);
        BOOST_CHECK(errors==0);
    }

    BOOST_AUTO_TEST_CASE(check_error)
    {
        BehaviourSubject<int> subject(200);

        int nexts{};
        int sum{};
        int errors{};
        int completes{};
        error::Error error;

        subject.OnNext(100);

        auto subscription = subject.AsObservable().Subscribe(
                [&](auto i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const error::Error &e) { ++errors; error = e; });

        subject.OnNext(1);
        subject.OnNext(2);
        subject.OnNext(3);
        subject.OnError(std::runtime_error("RT Err"));
        subject.OnNext(4);
        subject.OnCompleted();

        BOOST_CHECK(sum==106);
        BOOST_CHECK(completes==0);
        BOOST_CHECK(errors==1);

        BOOST_CHECK(error.IsType<std::runtime_error>());
        BOOST_CHECK(!error.IsType<std::exception>());
        BOOST_CHECK(std::string(error.Get<std::runtime_error>()->what()) == "RT Err");
    }

    BOOST_AUTO_TEST_CASE(subscribe_after_complete) {

        BehaviourSubject<int> subject(200);

        int nexts{};
        int sum{};
        int errors{};
        int completes{};
        error::Error error;

        subject.OnCompleted();

        auto subscription = subject.AsObservable().Subscribe(
                [&](auto i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const error::Error &e) { ++errors; error = e; });

        BOOST_CHECK(sum==200);
        BOOST_CHECK(completes==1);
        BOOST_CHECK(errors==0);
    }

    BOOST_AUTO_TEST_CASE(subscribe_after_error) {

        BehaviourSubject<int> subject(200);

        int nexts{};
        int sum{};
        int errors{};
        int completes{};
        error::Error error;

        subject.OnNext(10);
        subject.OnNext(100);
        subject.OnError(std::runtime_error("RT Err"));

        auto subscription = subject.AsObservable().Subscribe(
                [&](auto i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const error::Error &e) { ++errors; error = e; });

        BOOST_CHECK(sum==100);
        BOOST_CHECK(completes==0);
        BOOST_CHECK(errors==1);

        BOOST_CHECK(error.IsType<std::runtime_error>());
        BOOST_CHECK(!error.IsType<std::exception>());
        BOOST_CHECK(std::string(error.Get<std::runtime_error>()->what()) == "RT Err");
    }

    BOOST_AUTO_TEST_CASE(assing_move_subs)
    {
        BehaviourSubject<int> s0(0);
        BehaviourSubject<int> s1(1);
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;

        BOOST_CHECK((bool)s0);
        BOOST_CHECK((bool)s1);

        s0 = std::move(s1);
        std::shared_ptr<IObserver<int>> ps0 = s0;

        BOOST_CHECK((bool)s0);
        BOOST_CHECK(!(bool)s1);

        BOOST_CHECK(p_old_s1 == ps0);
    }

    BOOST_AUTO_TEST_CASE(move_ctor_subs)
    {
        BehaviourSubject<int> s1(1);
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;
        BOOST_CHECK((bool)s1);
        BehaviourSubject<int> s0(std::move(s1));

        BOOST_CHECK(!(bool)s1);
        BOOST_CHECK((bool)s0);

        std::shared_ptr<IObserver<int>> ps0 = s0;

        BOOST_CHECK(p_old_s1 == ps0);
    }

    BOOST_AUTO_TEST_CASE(self_move)
    {
        BehaviourSubject<int> s1(1);
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;
        BOOST_CHECK((bool)s1);
        s1 = std::move(s1);
        BOOST_CHECK((bool)s1);
        std::shared_ptr<IObserver<int>> p_new_s1 = s1;
        BOOST_CHECK(p_old_s1 == p_new_s1);
    }

BOOST_AUTO_TEST_SUITE_END()
