//
// Created by cds on 29/10/2020.
//

#include <srx/subject/Subject.hpp>

#include <memory>

#include <boost/test/unit_test.hpp>

using namespace srx::core;
using namespace srx::subject;
using namespace srx;

BOOST_AUTO_TEST_SUITE( SubjectSuite )

BOOST_AUTO_TEST_CASE(unsubscribe)
{
    int nexts{};
    int sum{};
    int errors{};
    int completes{};
    error::Error error;

    Subject<int> subject;

    auto subscription = subject.AsObservable().Subscribe(
            [&](auto i) {
                sum += i;
                ++nexts;
            },
            [&]() { ++completes; },
            [&](const auto &e) { ++errors; error = e; });

    subject.OnNext(1);
    subject.OnNext(2);
    subject.OnNext(3);
    subscription.Unsubscribe();
    subject.OnNext(4);
    subject.OnCompleted();

    BOOST_CHECK(sum==6);
    BOOST_CHECK(completes==0);
    BOOST_CHECK(errors==0);
}

BOOST_AUTO_TEST_CASE(complete)
{
    int nexts{};
    int sum{};
    int errors{};
    int completes{};
    error::Error error;

    Subject<int> subject;

    auto subscription = subject.AsObservable().Subscribe(
            [&](auto i) {
                sum += i;
                ++nexts;
            },
            [&]() { ++completes; },
            [&](const auto &e) { ++errors; error = e; });

    subject.OnNext(1);
    subject.OnNext(2);
    subject.OnNext(3);
    subject.OnCompleted();
    subject.OnNext(4);
    subject.OnError({});

    BOOST_CHECK(sum==6);
    BOOST_CHECK(completes==1);
    BOOST_CHECK(errors==0);
}

BOOST_AUTO_TEST_CASE(check_error)
{
    int nexts{};
    int sum{};
    int errors{};
    int completes{};
    error::Error error;

    Subject<int> subject;

    auto subscription = subject.AsObservable().Subscribe(
            [&](auto i) {
                sum += i;
                ++nexts;
            },
            [&]() { ++completes; },
            [&](const auto &e) { ++errors; error = e; });

    subject.OnNext(1);
    subject.OnNext(2);
    subject.OnNext(3);
    subject.OnError(std::runtime_error("RT Err"));
    subject.OnNext(4);
    subject.OnCompleted();

    BOOST_CHECK(sum==6);
    BOOST_CHECK(completes==0);
    BOOST_CHECK(errors==1);

    BOOST_CHECK(error.IsType<std::runtime_error>());
    BOOST_CHECK(!error.IsType<std::exception>());
    BOOST_CHECK(std::string(error.Get<std::runtime_error>()->what()) == "RT Err");
}

    BOOST_AUTO_TEST_CASE(assign_move_subs)
    {
        Subject<int> s0;
        Subject<int> s1;
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;


        BOOST_CHECK((bool)s0);
        BOOST_CHECK((bool)s1);

        s0 = std::move(s1);
        std::shared_ptr<IObserver<int>> ps0 = s0;

        BOOST_CHECK((bool)s0);
        BOOST_CHECK(!(bool)s1);

        BOOST_CHECK(p_old_s1 == ps0);
    }

    BOOST_AUTO_TEST_CASE(move_ctor_subs)
    {
        Subject<int> s1;
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;
        BOOST_CHECK((bool)s1);
        Subject<int> s0(std::move(s1));

        BOOST_CHECK(!(bool)s1);
        BOOST_CHECK((bool)s0);

        std::shared_ptr<IObserver<int>> ps0 = s0;

        BOOST_CHECK(p_old_s1 == ps0);
    }

    BOOST_AUTO_TEST_CASE(self_move)
    {
        Subject<int> s1;
        std::shared_ptr<IObserver<int>> p_old_s1 = s1;
        BOOST_CHECK((bool)s1);
        s1 = std::move(s1);
        BOOST_CHECK((bool)s1);
        std::shared_ptr<IObserver<int>> p_new_s1 = s1;
        BOOST_CHECK(p_old_s1 == p_new_s1);
    }

BOOST_AUTO_TEST_SUITE_END()
