//
// Created by chris on 25/10/2020.
//

#include <srx/subscription/SubscriptionGuard.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::subscription;

BOOST_AUTO_TEST_SUITE( SubscriptionGuardSuite )

    BOOST_AUTO_TEST_CASE(unsubscribe_actions_once)
    {
        size_t count{};
        auto inc = [&count]() { ++count; };
        {
            SubscriptionGuard s(Action(inc));
            BOOST_CHECK(count == 0);
        }
        BOOST_CHECK(count == 1);
    }

    BOOST_AUTO_TEST_CASE(unique_unsubscribe_actions_once)
    {
        size_t count{};
        auto inc = [&count]() { ++count; };
        {
            auto s = MakeSubscriptionGuard(Action(inc));
            BOOST_CHECK(count == 0);
        }
        BOOST_CHECK(count == 1);
    }

BOOST_AUTO_TEST_SUITE_END()
