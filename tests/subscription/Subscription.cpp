//
// Created by chris on 22/10/2020.
//

#include <srx/subscription/Subscription.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::subscription;

BOOST_AUTO_TEST_SUITE( SubscriptionSuite )

    BOOST_AUTO_TEST_CASE(default_subs_is_false)
    {
        Subscription s;
        BOOST_CHECK(!s);
    }

    BOOST_AUTO_TEST_CASE(empty_subs_is_true)
    {
        auto s = Empty();
        BOOST_CHECK(s);
    }

    BOOST_AUTO_TEST_CASE(action_subs_is_true)
    {
        auto s = Action([](){});
        BOOST_CHECK(s);
    }

    BOOST_AUTO_TEST_CASE(unsubscribing_makes_false)
    {
        auto s = Empty();
        BOOST_CHECK(s);
        s.Unsubscribe();
        BOOST_CHECK(!s);
    }

    BOOST_AUTO_TEST_CASE(move_ctor)
    {
        auto s = Empty();
        BOOST_CHECK(s);
        auto t = std::move(s);
        BOOST_CHECK(!s);
        BOOST_CHECK(t);
    }

    BOOST_AUTO_TEST_CASE(assign_ctor)
    {
        auto s = Empty();
        Subscription t;
        BOOST_CHECK(s);
        BOOST_CHECK(!t);
        t = std::move(s);
        BOOST_CHECK(!s);
        BOOST_CHECK(t);
    }

    BOOST_AUTO_TEST_CASE(self_assign)
    {
        auto s = Empty();
        BOOST_CHECK(s);
        s = std::move(s);
        BOOST_CHECK(s);
    }

    BOOST_AUTO_TEST_CASE(move_assign_doesnt_swap)
    {
        Subscription s;
        auto t = Empty();
        BOOST_CHECK(!s);
        BOOST_CHECK(t);
        t = std::move(s);
        BOOST_CHECK(!s);
        BOOST_CHECK(!t);
    }

    BOOST_AUTO_TEST_CASE(unsubscribe_actions_once)
    {
        size_t count {};
        auto inc = [&count](){++count;};
        auto s = Action(inc);
        BOOST_CHECK(count==0);
        s.Unsubscribe();
        BOOST_CHECK(count==1);
        s.Unsubscribe();
        BOOST_CHECK(count==1);
    }

    BOOST_AUTO_TEST_CASE(unsubscribe_moves)
    {
        size_t count {};
        auto inc = [&count](){++count;};
        auto s = Action(inc);
        auto t = std::move(s);
        BOOST_CHECK(count==0);
        s.Unsubscribe();
        BOOST_CHECK(count==0);
        t.Unsubscribe();
        BOOST_CHECK(count==1);
    }

    BOOST_AUTO_TEST_CASE(subs_reset)
    {
        size_t count {};
        auto inc = [&count](){++count;};
        auto s = Action(inc);
        BOOST_CHECK(s);
        s.reset();
        BOOST_CHECK(!s);
        s.Unsubscribe();
        BOOST_CHECK(count==0);
    }

BOOST_AUTO_TEST_SUITE_END()
