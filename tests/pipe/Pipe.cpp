//
// Created by chris on 27/10/2020.
//

#include <srx/pipe/Pipe.hpp>
#include <srx/operators/Take.hpp>
#include <srx/factory/Create.hpp>
#include <srx/core/Observable.hpp>

#include <boost/test/unit_test.hpp>

using namespace srx::pipe;
using namespace srx::core;
using namespace srx::operators;
using namespace srx::factory;
using namespace srx::error;

BOOST_AUTO_TEST_SUITE( PipeSuite )

BOOST_AUTO_TEST_CASE(take)
{
    {
        auto c = Create<int>([](auto o) {
            o->OnNext(1);
            o->OnNext(2);
            o->OnNext(3);
            o->OnNext(4);
            o->OnNext(5);
            o->OnError({});
        });

        auto observable = Pipe(c, Take<int>(2), Take<int>(4));

        int nexts{};
        int sum{};
        int errors{};
        int completes{};

        auto subscription = observable.Subscribe(
                [&](int i) {
                    sum += i;
                    ++nexts;
                },
                [&]() { ++completes; },
                [&](const Error &) { ++errors; });

        BOOST_CHECK(nexts == 2);
        BOOST_CHECK(sum == 3);
        BOOST_CHECK(errors == 0);
        BOOST_CHECK(completes == 1);

        subscription.Unsubscribe();
    }
}

BOOST_AUTO_TEST_SUITE_END()
