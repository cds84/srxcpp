//
// Created by cds on 28/10/2020.
//

#include <srx/error/Error.hpp>

#include <stdexcept>

#include <boost/test/unit_test.hpp>

using namespace srx::error;

BOOST_AUTO_TEST_SUITE( ErrorSuite )

    BOOST_AUTO_TEST_CASE(error_is_same)
    {
        auto e = Error(std::runtime_error("MyRuntimeError"));

        BOOST_CHECK(e.IsType<int>() == false);
        BOOST_CHECK(e.IsType<std::exception>() == false);
        BOOST_CHECK(e.IsType<std::runtime_error>() == true);
    }

    BOOST_AUTO_TEST_CASE(error_get)
    {
        auto err = Error(std::runtime_error("MyRuntimeError"));

        auto i = err.Get<int>();
        auto e = err.Get<std::exception>();
        auto r = err.Get<std::runtime_error>();

        BOOST_CHECK(i.has_value() == false);
        BOOST_CHECK(e.has_value() == false);
        BOOST_CHECK(r.has_value() == true);
    }

    BOOST_AUTO_TEST_CASE(error_throw)
    {
        int run_except{};
        int std_except{};
        int any{};

        try {
            Error(std::runtime_error("MyRuntimeError")).Throw();
        }
        catch(const std::runtime_error &ex) {
            ++run_except;
        }
        catch(const std::exception &ex) {
            ++std_except;
        }
        catch(...) {
            ++any;
        }
        BOOST_CHECK(run_except==1);
        BOOST_CHECK(std_except==0);
        BOOST_CHECK(any==0);
    }
BOOST_AUTO_TEST_SUITE_END()
