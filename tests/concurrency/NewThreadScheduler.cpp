//
// Created by cds on 18/11/2020.
//

#include <srx/concurrency/schedulers/NewThreadScheduler.hpp>

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <chrono>
#include <thread>
#include <future>
#include <set>
#include <mutex>

using namespace std::chrono_literals;

using namespace srx::concurrency;

BOOST_AUTO_TEST_SUITE( NewThreadScheduelerSuite )

    BOOST_AUTO_TEST_CASE(can_schedule_void_return)
    {
        std::mutex mutex;
        std::set<std::thread::id> threads;
        threads.insert(std::this_thread::get_id());
        const int thread_count=1000;
        std::promise<void> promise;
        std::future<void> future = promise.get_future();
        {
            auto s = NewThreadScheduler()();
            for (int i = 0; i < thread_count - 1; i++)
                s.Schedule([&]() {
                    {
                        std::scoped_lock lock(mutex);
                        threads.insert(std::this_thread::get_id());
                        if (threads.size() == thread_count)
                            promise.set_value();
                    }
                    future.wait(); // keep all threads alive to prevent re-use of a thread id.
                });
        }
        BOOST_CHECK(threads.size() == thread_count);
    }

BOOST_AUTO_TEST_SUITE_END()
