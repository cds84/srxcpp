//
// Created by cds on 11/11/2020.
//

#include <srx/concurrency/schedulers/ImmediateScheduler.hpp>


#include <boost/test/unit_test.hpp>

using namespace srx::concurrency;

BOOST_AUTO_TEST_SUITE( ImmediateScheduelerSuite )

    BOOST_AUTO_TEST_CASE(can_schedule_void_return)
    {
        auto is = ImmediateScheduler()();
        int count{};
        is.Schedule([&](){++count;});
        BOOST_CHECK(count==1);
    }

BOOST_AUTO_TEST_SUITE_END()
